"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by syed_ on 2/15/2017.
 */
var core_1 = require('@angular/core');
var movie_service_1 = require('../movie-service/movie.service');
var GenreSearchComponent = (function () {
    /*updateYear(){
        console.log(this.genre);
    }*/
    //@Input() year;
    function GenreSearchComponent(movieService) {
        this.movieService = movieService;
        this.genre = 'thriller';
        this.movieList = [];
    }
    GenreSearchComponent.prototype.updateYear = function () {
        var _this = this;
        this.movieService.searchByGenre(this.genre)
            .subscribe(function (movie) { return _this.movieList = movie; }, function (error) { return console.log(error); });
    };
    GenreSearchComponent = __decorate([
        core_1.Component({
            templateUrl: 'genre-search.component.html'
        }), 
        __metadata('design:paramtypes', [movie_service_1.MovieService])
    ], GenreSearchComponent);
    return GenreSearchComponent;
}());
exports.GenreSearchComponent = GenreSearchComponent;
//# sourceMappingURL=genre-search.component.js.map